# Les aventures héroïquement banales de Tix & Romaric

![Episode 10](tix_and_romaric/preview/010_chocopain.png "Le chocopain")

## Bandes-dessinées

L’univers de Tix & Romaric se décompose de deux bandes-dessinées distinctes:

### Tix & Romaric

La série principale met en scène Tix et sa bande de copains. La langue française
y est mise à mal grâce à l’utilisation judicieuse de jeux de mots vaseux et
d’expressions usuelles de la vie courante.

### Tilapin

Tilapin est un lapin qui comme tous les lagomorphes a la grande particularité
d’avoir un grand appétit. Bien que son estomac semble être le centre névralgique
de toutes ses réflexions personnelles, il a le mérite d’être muet comme une
carpe… qu’il raffole cela dit en passant.

## Licence

Les bandes-dessinées de ce dépôt sont disponibles sous la licence Art Libre.

http://artlibre.org/

La police d'écriture Fira est disponible sous licence OFL.

http://scripts.sil.org/OFL